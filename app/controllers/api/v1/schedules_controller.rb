module Api
  module V1
    class SchedulesController < ApiController
      before_action :authenticate_api_v1_user!
      
      def index
        t = Date.new(params[:year].to_i, params[:month].to_i, 1)

        if params[:user_id]
          render json: Schedule.includes(team: :jobs).where(active_on: t.beginning_of_month..t.end_of_month).where(user_id: params[:user_id])
        elsif params[:office_id]
          render json: Schedule.includes(team: :jobs).joins(:team).where(active_on: t.beginning_of_month..t.end_of_month).where("teams.office_id = ?", params[:office_id])
        end
      end

      def generate
        input_service = InputService.new(params[:year], params[:month])
        input_service.call
        File.open("input.txt", 'w') { |file| file.write(input_service.text) }

        `/opt/jdk1.8.0_65/bin/java -jar logic/out/artifacts/TCC2016_jar/TCC2016.jar input.txt`

        output_service = OutputService.new(
          File.open("output.txt").read.split("\n"), params[:year], params[:month])
        output_service.save

        render json: "", status: 200
      end
    end
  end
end
