# == Schema Information
#
# Table name: team_jobs
#
#  id      :integer          not null, primary key
#  team_id :integer
#  job_id  :integer
#

class TeamJob < ActiveRecord::Base
  belongs_to :team
  belongs_to :job
end
