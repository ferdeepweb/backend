class OfficeSerializer < ActiveModel::Serializer
  attributes :id, :name, :address, :max_teams, :teams

  has_many :teams
end
