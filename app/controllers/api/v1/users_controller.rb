module Api
  module V1
    class UsersController < ApiController
      before_action :authenticate_api_v1_user!

      def index
        render json: User.all
      end

      def create
        render json: User.create(user_params)
      end

      private

      def user_params
        params.permit(
          :first_name,
          :last_name,
          :email,
          :password,
          :role_id,
          :oib,
          :birthdate,
          :sex_id,
          :address,
          :location,
          :office_id,
          :home_phone,
          :mobile_phone,
          :note,
          job_ids: []
        )
      end
    end
  end
end
