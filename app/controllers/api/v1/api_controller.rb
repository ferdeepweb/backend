module Api
  module V1
    class ApiController < ApplicationController
      include DeviseTokenAuth::Concerns::SetUserByToken

      before_action :set_default_response_format

      protected

      def set_default_response_format
        request.format = :json
      end
    end
  end
end
