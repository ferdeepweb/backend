module Api
  module V1
    class OfficesController < ApiController
      before_action :authenticate_api_v1_user!

      def index
        render json: Office.all.includes(teams: :jobs)
      end

      def show
        render json: Office.find(params[:id]), root: false
      end

      def create
        render json: Office.create(office_params)
      end

      def update
        office = Office.find(params[:id])
        if office.update(office_params)
          render json: office
        else
          render json: office.errors, status: 422
        end
      end

      def destroy
        render json: Office.find(params[:id]).destory
      end

      def users
        render json: Office.find(params[:id]).users
      end

      private

      def office_params
        params.permit(:name, :address, :max_teams)
      end
    end
  end
end
