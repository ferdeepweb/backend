class AddDataToUsers < ActiveRecord::Migration
  def change
    add_column :users, :oib, :string
    add_column :users, :birthdate, :date
    add_column :users, :sex, :integer
    add_column :users, :address, :string
    add_column :users, :location, :string
    add_column :users, :office_id, :integer
    add_column :users, :home_phone, :string
    add_column :users, :mobile_phone, :string
    add_column :users, :note, :text
  end
end
