module Api
  module V1
    class TeamsController < ApiController
      before_action :authenticate_api_v1_user!

      def index
        render json: Team.all
      end

      def show
        render json: Team.find(params[:id])
      end

      def create
        render json: Team.create(team_params)
      end

      def update
        team = Team.find(params[:id])
        if team.update(team_params)
          render json: team
        else
          render json: team.errors, status: 422
        end
      end

      def destroy
        render json: Team.find(params[:id]).destroy
      end

      private

      def team_params
        params.permit(:office_id, :team_type_id, job_ids: [])
      end
    end
  end
end
