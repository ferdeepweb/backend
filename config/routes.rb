Rails.application.routes.draw do
  namespace :api do
    namespace :v1 do
      mount_devise_token_auth_for 'User', at: 'auth'
      resources :users
      resources :offices do
        get 'users', on: :member, to: 'offices#users'
      end
      resources :schedules
      resources :teams
      resources :user_statuses

      get 'generate', to: "schedules#generate"

      get 'work_reports', to: "reports#work", constraints: { format: 'pdf' }
      get 'overall_reports', to: "reports#overall", constraints: { format: 'pdf' }
    end
  end
end
