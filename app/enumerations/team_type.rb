class TeamType < Enumeration::Base
  values main: { id: 1, name: "Main" },
         side: { id: 2, name: "Side" }
end
