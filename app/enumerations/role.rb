class Role < Enumeration::Base
  values admin: { id: 1, name: 'Administrator' },
         employee: { id: 2, name: 'Employee' }
end
