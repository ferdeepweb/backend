class Sex < Enumeration::Base
  values male: { id: 1, name: "Male" },
         female: { id: 2, name: "Female" }
end
