# == Schema Information
#
# Table name: schedules
#
#  id        :integer          not null, primary key
#  team_id   :integer
#  active_on :date
#  shift_id  :integer
#  user_id   :integer
#  job_id    :integer
#

class Schedule < ActiveRecord::Base
  validates_presence_of :team_id, :active_on, :shift_id, :user_id, :job_id
  validates_uniqueness_of :active_on, scope: :user_id

  belongs_to :team
  enumeration :shift
  belongs_to :user
  belongs_to :job
end
