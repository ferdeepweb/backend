# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  provider               :string           default("email"), not null
#  uid                    :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string
#  last_sign_in_ip        :string
#  first_name             :string
#  last_name              :string
#  email                  :string
#  tokens                 :json
#  created_at             :datetime
#  updated_at             :datetime
#  role_id                :integer
#  oib                    :string
#  birthdate              :date
#  sex                    :integer
#  address                :string
#  location               :string
#  office_id              :integer
#  home_phone             :string
#  mobile_phone           :string
#  note                   :text
#  sex_id                 :integer
#

class User < ActiveRecord::Base
  devise :database_authenticatable, :registerable,
          :recoverable, :rememberable, :trackable, :validatable, :omniauthable
  include DeviseTokenAuth::Concerns::User

  enumeration :role
  enumeration :sex

  validates_presence_of :first_name, :last_name, :role_id, :oib, :birthdate,
                        :sex_id, :address, :location, :office_id, :home_phone,
                        :mobile_phone

  belongs_to :office

  has_many :user_jobs
  has_many :jobs, through: :user_jobs
  has_many :schedules
  has_many :user_statuses
  has_many :statuses, through: :user_statuses
end
