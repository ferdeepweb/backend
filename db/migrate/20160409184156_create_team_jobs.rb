class CreateTeamJobs < ActiveRecord::Migration
  def change
    create_table :team_jobs do |t|
      t.integer :team_id
      t.integer :job_id
    end
  end
end
