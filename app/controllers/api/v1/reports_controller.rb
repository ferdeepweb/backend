module Api
  module V1
    class ReportsController < ApiController
      before_action :set_default_response_format
      def work
        kit = ::PDFKit.new(
          render_to_string(
            'api/v1/reports/work',
            formats: [:html],
            locals: {
              working_hours: ReportWorkQuery.new(Date.parse(params[:start_date]), Date.parse(params[:end_date])).call,
              start_date: params[:start_date],
              end_date: params[:end_date]
            }
          )
        )
        kit.stylesheets << 'app/assets/stylesheets/bootstrap.css'

        send_data kit.to_pdf, filename: 'working_hours.pdf', disposition: :inline, type: 'application/pdf'
      end

      def overall
        query = ReportOverallQuery.new(Date.parse(params[:start_date]), Date.parse(params[:end_date]))
        query.call

        kit = ::PDFKit.new(
          render_to_string(
            'api/v1/reports/overall',
            formats: [:html],
            locals: {
              vacations: query.vacations,
              sick_leaves: query.sick_leaves,
              start_date: params[:start_date],
              end_date: params[:end_date]
            }
          )
        )
        kit.stylesheets << 'app/assets/stylesheets/bootstrap.css'

        send_data kit.to_pdf, filename: 'overall.pdf', disposition: :inline, type: 'application/pdf'
      end

      protected

      def set_default_response_format
        request.format = :html
      end
    end
  end
end
