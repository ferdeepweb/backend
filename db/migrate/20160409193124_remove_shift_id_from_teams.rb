class RemoveShiftIdFromTeams < ActiveRecord::Migration
  def change
    remove_column :teams, :shift_id
    remove_column :teams, :active_on
  end
end
