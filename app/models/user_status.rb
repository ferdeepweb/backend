# == Schema Information
#
# Table name: user_statuses
#
#  id        :integer          not null, primary key
#  user_id   :integer
#  status_id :integer
#  date      :date
#

class UserStatus < ActiveRecord::Base
  belongs_to :user
  belongs_to :status
end
