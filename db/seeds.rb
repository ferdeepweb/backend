# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

ActiveRecord::Base.connection.reset_pk_sequence!('jobs')
ActiveRecord::Base.connection.reset_pk_sequence!('users')
ActiveRecord::Base.connection.reset_pk_sequence!('statuses')

Job.create([
  {name: "Dispatcher"},
  {name: "Doctor"},
  {name: "Technician"},
  {name: "Driver"}
])

Status.create([
  { name: "Available" },
  { name: "Day Off" },
  { name: "Vacation" },
  { name: "Sick Leave" }
])

number_of_offices = 4
number_of_offices.times do
  Office.create(
    name: Faker::Commerce.department,
    address: Faker::Address.street_address,
    max_teams: rand(2) + 1
  )
end

nubmer_of_admins = 2
User.create(
  first_name: "Matej",
  last_name: "Serbec",
  email: "matej@vibby.com",
  password: "Password1234",
  role_id: 1,
  oib: Faker::Number.number(13),
  birthdate: Faker::Date.between(40.years.ago, 20.years.ago),
  sex_id: rand(2) + 1,
  address: Faker::Address.street_address,
  location: Faker::Address.city,
  office_id: rand(number_of_offices) + 1,
  home_phone: Faker::Number.number(13),
  mobile_phone: Faker::Number.number(13),
  note: Faker::Lorem.paragraph,
)
nubmer_of_admins.times do
  User.create(
    first_name: Faker::Name.first_name,
    last_name: Faker::Name.last_name,
    email: Faker::Internet.email,
    password: "Password1234",
    role_id: 1,
    oib: Faker::Number.number(13),
    birthdate: Faker::Date.between(40.years.ago, 20.years.ago),
    sex_id: rand(2) + 1,
    address: Faker::Address.street_address,
    location: Faker::Address.city,
    office_id: rand(number_of_offices) + 1,
    home_phone: Faker::Number.number(13),
    mobile_phone: Faker::Number.number(13),
    note: Faker::Lorem.paragraph,
  )
end

number_of_employees = 30
number_of_employees.times do
  user = User.create(
    first_name: Faker::Name.first_name,
    last_name: Faker::Name.last_name,
    email: Faker::Internet.email,
    password: "Password1234",
    role_id: 2,
    oib: Faker::Number.number(13),
    birthdate: Faker::Date.between(40.years.ago, 20.years.ago),
    sex_id: rand(2) + 1,
    address: Faker::Address.street_address,
    location: Faker::Address.city,
    office_id: rand(number_of_offices) + 1,
    home_phone: Faker::Number.number(13),
    mobile_phone: Faker::Number.number(13),
    note: Faker::Lorem.paragraph,
  )

  (1..Job.count).to_a.sort{ rand() - 0.5 }[0..(rand(Job.count) + 1)].each do |id|
    UserJob.create(user_id: user.id, job_id: id)
  end

end

Office.all.each do |office|
  if office.max_teams >= 1
    team = Team.create(office_id: office.id, team_type_id: 1)
    (1..Job.count).to_a.sort{ rand() - 0.5 }[1..(rand(Job.count) + 1)].each do |id|
      TeamJob.create(team_id: team.id, job_id: id)
    end
  end

  if office.max_teams == 2
    team = Team.create(office_id: office.id, team_type_id: 2)
    (1..Job.count).to_a.sort{ rand() - 0.5 }[1..(rand(Job.count) + 1)].each do |id|
      TeamJob.create(team_id: team.id, job_id: id)
    end
  end
end

(90.days.ago.to_date..90.days.from_now.to_date).to_a.each do |d|
  # Team.all.each do |team|
  #   office = team.office
  #   office.users.sort{ rand() - 0.5 }[0...rand(Job.count)].each do |user|
  #     Schedule.create(team_id: team.id, active_on: d, shift_id: rand(2) + 1, user_id: user.id, job_id: rand(Job.count) + 1)
  #   end
  # end

  User.where(role_id: Role.employee).each do |user|
    UserStatus.create(user_id: user.id, date: d, status_id: rand(Status.count) + 1)
  end
end
