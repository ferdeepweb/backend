module Api
  module V1
    class UserStatusesController < ApiController
      before_action :authenticate_api_v1_user!
      
      def index
        render json: UserStatus.all
      end

      def show
        render json: UserStatus.find(params[:user_id])
      end

      def create
        render json: UserStatus.create(user_params)
      end

      def update
        user_status = UserStatus.find_by(user_id: params[:user_id], date: params[:date])
        if user_status.update(user_params)
          render json: user_status
        else
          render json: user_status.errors
        end
      end

      def destroy
        render json: UserStatus.find_by(user_id: params[:user_id], date: params[:date]).destroy
      end

      private

      def user_params
        params.permit(:user_id, :date, :status_id)
      end
    end
  end
end
