class CreateSchedules < ActiveRecord::Migration
  def change
    create_table :schedules do |t|
      t.integer :team_id
      t.date :active_on
      t.integer :shift_id
      t.integer :job_id
    end
  end
end
