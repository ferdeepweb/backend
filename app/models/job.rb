# == Schema Information
#
# Table name: jobs
#
#  id   :integer          not null, primary key
#  name :string
#

class Job < ActiveRecord::Base
  validates_presence_of :name
end
