class CreateTeams < ActiveRecord::Migration
  def change
    create_table :teams do |t|
      t.integer :office_id
      t.integer :shift_id
      t.integer :team_type_id
      t.date :active_on

      t.timestamps null: false
    end
  end
end
