class OutputService
  @@SHIFT_MAP = {
    "D" => 1,
    "N" => 2
  }

  @@JOB_MAP = {
    "D" => 1,
    "M" => 2,
    "T" => 3,
    "V" => 4
  }

  def initialize(text, year, month)
    @text = text
    @year = year.to_i
    @month = month.to_i
  end

  def save
    date = Date.new(@year, @month, 1)
    Schedule.where(active_on: date.beginning_of_month..date.end_of_month).destroy_all

    User.where(role_id: Role.employee).each_with_index do |user, u_index|
      text_dates = @text[u_index].split(" ")
      text_dates.each_with_index do |text_date, d_index|
        next if text_date == "_"

        s_date = text_date.split("-")
        office_id = s_date[0].to_i + 1
        team_type_id = s_date[1].to_i + 1
        shift_id = @@SHIFT_MAP[s_date[2]]
        job_id = @@JOB_MAP[s_date[3]]

        team = Team.find_by(office_id: office_id, team_type_id: team_type_id)

        Schedule.create(active_on: date + d_index, team_id: team.id, shift_id: shift_id, job_id: job_id, user_id: user.id)
      end
    end
  end
end
