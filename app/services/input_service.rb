class InputService
  attr_reader :year, :month, :text

  @@JOB_MAP = {
    "Dispatcher" => "D",
    "Doctor" => "M",
    "Technician" => "T",
    "Driver" => "V"
  }

  @@STATUS_MAP = {
    "Available" => "D",
    "Day Off" => "S",
    "Vacation" => "G",
    "Sick Leave" => "B"
  }

  def initialize(year, month)
    @year = year.to_i
    @month = month.to_i
    @text = ""
  end

  def call
    number_of_weeks = (Time::days_in_month(@month, @year).to_f / 7).to_i
    @text << number_of_weeks.to_s << "\n"

    @text << Office.count.to_s << "\n"
    Office.all.each do |office|
      specify_office_teams(office)
    end

    users = User.where(role_id: Role.employee)
    @text << users.size.to_s << "\n"
    users.each do |user|
      specify_user_preferences(user)
    end
  end

  private

  def specify_office_teams(office)
    office.teams.each do |team|
      team.jobs.each do |job|
        @text << @@JOB_MAP[job.name]
      end
      @text << ' '
    end
    @text << "\n"
  end

  def specify_user_preferences(user)
    @text << (user.office_id - 1).to_s << ' '
    user.jobs.each do |job|
      @text << @@JOB_MAP[job.name]
    end
    @text << ' '

    date = Date.new(@year, @month, 1)
    user_statuses = user.user_statuses.includes(:status).where(date: date.beginning_of_month..date.end_of_month)[0...28]
    user_statuses.each do |user_status|
      @text << @@STATUS_MAP[user_status.status.name]
    end
    @text << "\n"
  end
end
