class Shift < Enumeration::Base
  values day: { id: 1, name: "Day" },
         night: { id: 2, name: "Night" }
end
