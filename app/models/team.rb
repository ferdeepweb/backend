# == Schema Information
#
# Table name: teams
#
#  id           :integer          not null, primary key
#  office_id    :integer
#  team_type_id :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

class Team < ActiveRecord::Base
  validates_presence_of :office_id, :team_type_id
  validates_uniqueness_of :office_id, scope: :team_type_id

  before_save :max_teams_check

  enumeration :team_type

  belongs_to :office
  has_many :team_jobs
  has_many :jobs, through: :team_jobs

  private

  def max_teams_check
    if self.office.max_teams <= self.office.teams.size
      self.errors.add(:office_id, "This office has too many teams than allowed")
    end
  end
end
