# == Schema Information
#
# Table name: offices
#
#  id         :integer          not null, primary key
#  name       :string
#  address    :string
#  max_teams  :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Office < ActiveRecord::Base
  validates_presence_of :name, :address, :max_teams
  validates :max_teams, numericality: { greater_than: 0, less_than_or_equal_to: 2 }

  has_many :users
  has_many :teams
end
