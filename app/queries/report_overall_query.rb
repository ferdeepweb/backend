class ReportOverallQuery
  attr_reader :vacations, :sick_leaves

  def initialize(start_date, end_date)
    @start_date = start_date
    @end_date = end_date
  end

  def call
    @vacations = {}
    @sick_leaves = {}

    User.where(role_id: Role.employee).each do |user|
      @vacations[user] = UserStatus.where(user_id: user.id).where(date: @start_date..@end_date).where(status_id: 3).size
    end

    User.where(role_id: Role.employee).each do |user|
      @sick_leaves[user] = UserStatus.where(user_id: user.id).where(date: @start_date..@end_date).where(status_id: 4).size
    end
  end
end
