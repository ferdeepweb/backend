class ScheduleSerializer < ActiveModel::Serializer
  attributes :team, :active_on, :shift_id, :user, :job

  has_one :team
end
