class ReportWorkQuery
  attr_reader :start_date, :end_date

  def initialize(start_date, end_date)
    @start_date = start_date
    @end_date = end_date
    @working_hours = {}
  end

  def call
    User.includes(:office).where(role_id: Role.employee).each do |user|
      @working_hours[user] = 12 * Schedule.where(user_id: user.id).where(active_on: @start_date..@end_date).size
    end

    @working_hours
  end
end
