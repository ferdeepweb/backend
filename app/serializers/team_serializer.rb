class TeamSerializer < ActiveModel::Serializer
  attributes :id, :office_id, :team_type_id

  has_many :jobs, through: :team_jobs
end
