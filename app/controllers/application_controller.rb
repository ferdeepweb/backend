class ApplicationController < ActionController::Base
  include DeviseTokenAuth::Concerns::SetUserByToken

  protect_from_forgery with: :null_session

  before_action :configure_permitted_parameters, if: :devise_controller?

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up) << [
      :first_name,
      :last_name,
      :role_id,
      :oib,
      :birthdate,
      :sex_id,
      :address,
      :location,
      :office_id,
      :home_phone,
      :mobile_phone,
      :note, job_ids: []
    ]
  end
end
